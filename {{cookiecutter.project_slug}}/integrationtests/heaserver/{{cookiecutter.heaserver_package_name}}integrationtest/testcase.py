"""
Creates a test case class for use with the unittest library that is built into Python.
"""

from heaserver.service.testcase.integrationmockmongotestcase import get_test_case_cls_default
from heaserver.{{ cookiecutter.heaserver_package_name }} import service
from heaobject.user import NONE_USER
from heaserver.service.testcase.expectedvalues import ActionSpec

db_store = {
    service.MONGODB_{{ cookiecutter.heaobject_class_name | upper }}_COLLECTION: [{
        'id': '666f6f2d6261722d71757578',
        'created': None,
        'derived_by': None,
        'derived_from': [],
        'description': None,
        'display_name': 'Reximus',
        'invited': [],
        'modified': None,
        'name': 'reximus',
        'owner': NONE_USER,
        'shared_with': [],
        'source': None,
        'type': 'heaobject.{{ cookiecutter.heaobject_module_name}}.{{ cookiecutter.heaobject_class_name }}',
        'version': None
    },
        {
            'id': '0123456789ab0123456789ab',
            'created': None,
            'derived_by': None,
            'derived_from': [],
            'description': None,
            'display_name': 'Luximus',
            'invited': [],
            'modified': None,
            'name': 'luximus',
            'owner': NONE_USER,
            'shared_with': [],
            'source': None,
            'type': 'heaobject.{{ cookiecutter.heaobject_module_name}}.{{ cookiecutter.heaobject_class_name }}',
            'version': None
        }]}

TestCase = get_test_case_cls_default(coll=service.MONGODB_{{ cookiecutter.heaobject_class_name | upper }}_COLLECTION,
                                     href='http://localhost:8080/{{ cookiecutter.heaobject_class_name | lower }}s/',
                                     wstl_package=service.__package__,
                                     fixtures=db_store,
                                     get_actions=[ActionSpec(name='{{ cookiecutter.project_slug }}-{{ cookiecutter.heaobject_class_name | lower }}-get-properties',
                                                             rel=['properties']),
                                                  ActionSpec(name='{{ cookiecutter.project_slug }}-{{ cookiecutter.heaobject_class_name | lower }}-open',
                                                             url='/{{ cookiecutter.heaobject_class_name | lower }}s/{id}/opener',
                                                             rel=['opener']),
                                                  ActionSpec(name='{{ cookiecutter.project_slug }}-{{ cookiecutter.heaobject_class_name | lower }}-duplicate',
                                                             url='/{{ cookiecutter.heaobject_class_name | lower }}s/{id}/duplicator',
                                                             rel=['duplicator'])
                                                  ],
                                     get_all_actions=[ActionSpec(name='{{ cookiecutter.project_slug }}-{{ cookiecutter.heaobject_class_name | lower }}-get-properties',
                                                             rel=['properties']),
                                                      ActionSpec(name='{{ cookiecutter.project_slug }}-{{ cookiecutter.heaobject_class_name | lower }}-open',
                                                                 url='/{{ cookiecutter.heaobject_class_name | lower }}s/{id}/opener',
                                                                 rel=['opener']),
                                                      ActionSpec(name='{{ cookiecutter.project_slug }}-{{ cookiecutter.heaobject_class_name | lower }}-duplicate',
                                                                 url='/{{ cookiecutter.heaobject_class_name | lower }}s/{id}/duplicator',
                                                                 rel=['duplicator'])],
                                     duplicate_action_name='{{ cookiecutter.project_slug }}-{{ cookiecutter.heaobject_class_name | lower }}-duplicate-form')
